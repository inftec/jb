<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	
	<prerequisites>
	    <maven>3.0.4</maven>	    
	</prerequisites>
	
	<groupId>ch.inftec</groupId>
	<artifactId>jb</artifactId>
	<version>3.2</version>
	<packaging>pom</packaging>

	<name>JB</name>
	<description>InfTec parent POM. Contains plugin versions as well as profiles to deploy projects to Central and Inftec Nexus</description>

	<url>https://bitbucket.org/inftec/jb</url>
	<licenses>
		<license>
			<name>The Apache Software License, Version 2.0</name>
			<url>http://www.apache.org/licenses/LICENSE-2.0.txt</url>
			<distribution>repo</distribution>
		</license>
	</licenses>
	
	<scm>
    	<url>git@bitbucket.org:inftec/jb.git</url>
    	<connection>scm:git:git@bitbucket.org:inftec/jb.git</connection>
    	<developerConnection>scm:git:git@bitbucket.org:inftec/jb.git</developerConnection>
  	</scm>
  	
  	<developers>
  		<developer>
  			<id>mme</id>
  			<name>Martin Meyer</name>
  			<email>martin.meyer@inftec.ch</email>
  		</developer>
		<developer>
			<id>rotscher</id>
			<name>Roger Brechbühl</name>
			<email>roger.brechbuehl@inftec.ch</email>
		</developer>
  	</developers>

	<properties>
		<!-- Explicit declaration of Source Encoding -->
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<!-- Explicit declaration of reporting output encoding (test reports) -->
		<project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
		
		<!-- Defines the format of maven.build.timestamp that can be used for the manifest with maven-jar-plugin, for instance -->
		<maven.build.timestamp.format>yyyyMMdd-HHmmss</maven.build.timestamp.format>
		
		<!-- Define default tag by property so we could overwrite it -->
		<scmTag>release/${project.version}</scmTag>

		<!-- JU Plugin versions -->
		<version.ju-maven-plugin>4.4-5</version.ju-maven-plugin>
				
		<!-- Default Plugin versions -->
		<version.maven-clean-plugin>2.5</version.maven-clean-plugin>
		<version.maven-compiler-plugin>3.1</version.maven-compiler-plugin>
		<version.maven-deploy-plugin>2.8.2</version.maven-deploy-plugin>
		<version.maven-install-plugin>2.5</version.maven-install-plugin>
		<version.maven-jar-plugin>2.4</version.maven-jar-plugin>
		<version.maven-ejb-plugin>2.4</version.maven-ejb-plugin>
		<version.maven-war-plugin>2.4</version.maven-war-plugin>
		<version.maven-ear-plugin>2.9.1</version.maven-ear-plugin>
		<version.maven-resources-plugin>2.6</version.maven-resources-plugin>
		<version.maven-site-plugin>3.3</version.maven-site-plugin>
		<version.maven-javadoc-plugin>2.9.1</version.maven-javadoc-plugin>
		<version.maven-eclipse-plugin>2.9</version.maven-eclipse-plugin>
		<version.maven-source-plugin>2.2.1</version.maven-source-plugin>
		<version.maven-surefire-plugin>2.16</version.maven-surefire-plugin>
		<version.maven-failsafe-plugin>2.17</version.maven-failsafe-plugin>
		<version.maven-scm-plugin>1.9.2</version.maven-scm-plugin>
		<version.version-maven-plugin>2.1</version.version-maven-plugin>
		<version.nexus-staging-maven-plugin>1.6.4</version.nexus-staging-maven-plugin>
		<version.build-helper-maven-plugin>1.8</version.build-helper-maven-plugin>
		<version.cargo-plugin>1.4.10</version.cargo-plugin>
		<version.jboss-as-maven-plugin>7.5.Final</version.jboss-as-maven-plugin>
		<version.maven-enforcer-plugin>1.3.1</version.maven-enforcer-plugin>
		<version.maven-release-plugin>2.5.1</version.maven-release-plugin>
		<version.maven-gpg-plugin>1.4</version.maven-gpg-plugin>
	</properties>

	<build>
	    <pluginManagement>
	        <plugins>
	            <!-- Maven core plugins -->
	            <plugin>
	       			<artifactId>maven-clean-plugin</artifactId>
	            	<version>${version.maven-clean-plugin}</version>
	            </plugin>
	            <plugin>
	       			<artifactId>maven-compiler-plugin</artifactId>
	            	<version>${version.maven-compiler-plugin}</version>
	            </plugin>
	            <plugin>
	       			<artifactId>maven-deploy-plugin</artifactId>
	            	<version>${version.maven-deploy-plugin}</version>
	            	
	            	<!-- Deploy all artifacts at the end, not in the deploy phase of each module -->
	            	<configuration>
	            		<deployAtEnd>true</deployAtEnd>
	            	</configuration>
	            </plugin>
	            <plugin>
	       			<artifactId>maven-install-plugin</artifactId>
	            	<version>${version.maven-install-plugin}</version>
	            </plugin>
	            <plugin>
	       			<artifactId>maven-jar-plugin</artifactId>
	            	<version>${version.maven-jar-plugin}</version>
	            	
	            	<!-- Add manifest with build info by default -->
	            	<configuration>
		            	<archive>
							<manifest>
								<addDefaultImplementationEntries>true</addDefaultImplementationEntries>
							</manifest>
							<manifestEntries>
								<Build-Time>${maven.build.timestamp}</Build-Time>
							</manifestEntries>
						</archive>
					</configuration>
	            </plugin>
	            <plugin>
	       			<artifactId>maven-ejb-plugin</artifactId>
	            	<version>${version.maven-ejb-plugin}</version>
	            </plugin>
	            <plugin>
	       			<artifactId>maven-war-plugin</artifactId>
	            	<version>${version.maven-war-plugin}</version>
	            </plugin>
	            <plugin>
	       			<artifactId>maven-ear-plugin</artifactId>
	            	<version>${version.maven-ear-plugin}</version>
	            </plugin>
	            <plugin>
	       			<artifactId>maven-resources-plugin</artifactId>
	            	<version>${version.maven-resources-plugin}</version>
	            </plugin>
	            <plugin>
	       			<artifactId>maven-site-plugin</artifactId>
	            	<version>${version.maven-site-plugin}</version>
	            </plugin>
	            <plugin>
	       			<artifactId>maven-surefire-plugin</artifactId>
	            	<version>${version.maven-surefire-plugin}</version>
	            </plugin>
	            <plugin>
	       			<artifactId>maven-failsafe-plugin</artifactId>
	            	<version>${version.maven-failsafe-plugin}</version>
	            </plugin>
	            <plugin>
	       			<artifactId>maven-javadoc-plugin</artifactId>
	            	<version>${version.maven-javadoc-plugin}</version>
	            </plugin>
	            <plugin>
	       			<artifactId>maven-scm-plugin</artifactId>
	            	<version>${version.maven-scm-plugin}</version>
	            </plugin>
	            
	            <!-- More plugins -->
	           	<plugin>
	            	<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-release-plugin</artifactId>
					<version>${version.maven-release-plugin}</version>
					
					<configuration>
						<mavenExecutorId>forked-path</mavenExecutorId>
						<useReleaseProfile>false</useReleaseProfile>
						<arguments>-Psonatype-oss-release</arguments>
					</configuration>
				</plugin>
	            <plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-enforcer-plugin</artifactId>
					<version>${version.maven-enforcer-plugin}</version>
				</plugin>
	            <plugin>
	                <groupId>org.codehouse.mojo</groupId>
	                <artifactId>build-helper-maven-plugin</artifactId>
	                <version>${version.build-helper-maven-plugin}</version>	                
	            </plugin>
	            <plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-eclipse-plugin</artifactId>
					<version>${version.maven-eclipse-plugin}</version>
					
					<!-- Avoid problem with Eclipse plugin -->
					<configuration>
						<!--
						<downloadSources>true</downloadSources>
						<downloadJavadocs>true</downloadJavadocs>
						<alwaysWriteProjectConfig>true</alwaysWriteProjectConfig>
						-->
						<eclipseProjectDir>.</eclipseProjectDir>
					</configuration>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-source-plugin</artifactId>
					<version>${version.maven-source-plugin}</version>
				</plugin>
				<plugin>
					<groupId>org.codehaus.mojo</groupId>
					<artifactId>versions-maven-plugin</artifactId>
					<version>${version.version-maven-plugin}</version>
				</plugin>
				<plugin>
					<groupId>org.codehaus.cargo</groupId>
					<artifactId>cargo-maven2-plugin</artifactId>
					<version>${version.cargo-plugin}</version>
				</plugin>
				<plugin>
	                <groupId>org.jboss.as.plugins</groupId>
	                <artifactId>jboss-as-maven-plugin</artifactId>
	                <version>${version.jboss-as-maven-plugin}</version>
	            </plugin>
	            <plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-gpg-plugin</artifactId>
					<version>${version.maven-gpg-plugin}</version>
				</plugin>
	        </plugins>	        
	    </pluginManagement>
	    
		<plugins>
			<!-- Set default Java version to 7 -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<configuration>
					<source>1.7</source>
					<target>1.7</target>
				</configuration>
			</plugin>
			
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-jar-plugin</artifactId>
				<configuration>
					<skipIfEmpty>true</skipIfEmpty>
				</configuration>
				<executions>
					<execution>
						<goals>
							<goal>test-jar</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<artifactId>maven-scm-plugin</artifactId>
				<configuration>
					<tag>${scmTag}</tag>
				</configuration>
  			</plugin>
			<plugin>
				<!-- Versions Plugin that can be used to manage maven versions in various ways -->
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>versions-maven-plugin</artifactId>
				<configuration>
					<generateBackupPoms>false</generateBackupPoms>
				</configuration>
			</plugin>
		</plugins>
	</build>
	
	<profiles>
		<!-- Define release and snapshot repository for Sonatype -->
		<profile>
			<id>jb-sonatypeDistribution</id>
			
			<activation>
				<property>
					<name>jbPerformRelease</name>
					<value>sonatype</value>
				</property>
			</activation>
			
			<properties>
				<sonatypeOssDistMgmtSnapshotsUrl>https://oss.sonatype.org/content/repositories/snapshots/</sonatypeOssDistMgmtSnapshotsUrl>
			</properties>
			
			<distributionManagement>
				<snapshotRepository>
					<id>sonatype-nexus-snapshots</id>
					<name>Sonatype Nexus Snapshots</name>
					<url>${sonatypeOssDistMgmtSnapshotsUrl}</url>
				</snapshotRepository>
				<repository>
					<id>sonatype-nexus-staging</id>
					<name>Nexus Release Repository</name>
					<url>https://oss.sonatype.org/service/local/staging/deploy/maven2/</url>
				</repository>
			</distributionManagement>
		</profile>

		<!-- used for creating pre-releases which are published to inftec nexus -->
		<profile>
			<id>jb-inftecDistribution</id>

			<activation>
				<property>
					<name>jbPerformRelease</name>
					<value>inftec</value>
				</property>
			</activation>

			<distributionManagement>
				<snapshotRepository>
					<id>snapshots</id>
					<name>Inftec Snapshots</name>
					<url>https://mvn.inftec.ch/nexus/content/repositories/snapshots</url>
				</snapshotRepository>
				<repository>
					<id>inftec-nightly</id>
					<name>Inftec Nightly Release Repository</name>
					<url>https://mvn.inftec.ch/nexus/content/repositories/inftec-nightly</url>
				</repository>
			</distributionManagement>
		</profile>
		
    	<!-- Includes source artifacts in deployment -->
    	<profile>
    		<id>jb-attachSourceArtifacts</id>
    	
    		<activation>
    			<property>
					<name>jbPerformRelease</name>
				</property>
    		</activation>
    		
    		<build>
    			<plugins>
    				<!-- Include sources, including test JARs -->
					<plugin>
						<groupId>org.apache.maven.plugins</groupId>
						<artifactId>maven-source-plugin</artifactId>
						<executions>
							<execution>
								<goals>
									<goal>jar</goal>
									<goal>test-jar</goal>
								</goals>
							</execution>
						</executions>
					</plugin>
    			</plugins>
    		</build>
    	</profile>
    	
    	<!-- Sign the release arficates with GnuPG -->
		<profile>
			<id>jb-signReleaseArtifacts</id>
			
			<activation>
				<property>
					<name>jbPerformRelease</name>
				</property>
			</activation>
			
			<properties>
				<!-- Encrypted by InfTec Maven encryption key, can be decrypted by InfTec Bamboo server. -->
				<jbGpgPassphrase>ENC(lyHWB4FxF7ctITydcs6KuS0nsk2j0u/5)</jbGpgPassphrase>
				
				<!-- Default decryption key location for Bamboo Build -->
				<jbDecryptionKeyFile>/home/bamboo/jb-decryption.key</jbDecryptionKeyFile>
			</properties>
			
			<build>
				<plugins>
   					<plugin>
    					<groupId>ch.inftec.ju</groupId>
    					<artifactId>ju-maven-plugin</artifactId>
    					<version>${version.ju-maven-plugin}</version>
    					
		    			<configuration>
							<chain>
<!-- 								<silentlyIgnoreMissingResourceFolder>true</silentlyIgnoreMissingResourceFolder> -->
<!-- 								<resourceFolders> -->
<!-- 									<resourceFolder>esw-lib/src/main/resources/mvnProperties</resourceFolder> -->
<!-- 									Needed for resource lookup in modules... -->
<!-- 									<resourceFolder>../esw-lib/src/main/resources/mvnProperties</resourceFolder> -->
<!-- 								</resourceFolders> -->
								<logAddedKeys>true</logAddedKeys>
<!-- 								<filter> -->
<!-- 									<includes> -->
<!-- 										<include>esw.*</include> -->
<!-- 										<include>ju.*</include> -->
<!-- 										<include>liquibase.*</include> -->
<!-- 										<include>groups</include> -->
<!-- 									</includes> -->
<!-- 								</filter> -->
								<chainElements>
									<chainElement>
										<!-- Add System Properties so we can overwrite the jb.decryptorKeyFile by -D... -->
										<systemProperties/>
									</chainElement>
								</chainElements>
								<decryptor>
									<keyFilePathPropertyName>jb.decryptorKeyFile</keyFilePathPropertyName>
									<!-- If no decryption is set, we can still set the gpg.passphrase by -D... -->
									<ignoreMissingDecryption>true</ignoreMissingDecryption>
								</decryptor>
								<properties>
									<property>
										<key>jb.decryptorKeyFile</key>
										<value>${jbDecryptionKeyFile}</value>
									</property>
									<property>
										<key>gpg.passphrase</key>
										<value>${jbGpgPassphrase}</value>
									</property>
								</properties>
							</chain>
						</configuration>
    					
    					<executions>
	    					<execution>
	    						<id>load-properties</id>
	    						<goals><goal>properties</goal></goals>
	    						<phase>post-integration-test</phase>
	    					</execution>
	    				</executions>
    				</plugin>
					<plugin>
						<groupId>org.apache.maven.plugins</groupId>
						<artifactId>maven-gpg-plugin</artifactId>
						<configuration>
							<defaultKeyring>false</defaultKeyring>
							<!-- gpg.passphrase will be set by ju-maven-property -->
						</configuration>
						<executions>
							<execution>
								<id>sign-artifacts</id>
								<phase>verify</phase>
								<goals>
									<goal>sign</goal>
								</goals>
							</execution>
						</executions>
					</plugin>
				</plugins>
			</build>
		</profile>
		
		<!-- Attach JavaDocs to the release artifacts -->
		<profile>
			<id>jb-attachJavadocArtifacts</id>
			<activation>
				<property>
					<name>jbPerformRelease</name>
				</property>
			</activation>
			<build>
				<plugins>
					<plugin>
						<groupId>org.apache.maven.plugins</groupId>
						<artifactId>maven-javadoc-plugin</artifactId>
						<executions>
							<execution>
								<id>attach-javadocs</id>
								<goals>
									<goal>jar</goal>
								</goals>
							</execution>
						</executions>
					</plugin>
				</plugins>
			</build>
		</profile>
		
		<!-- Include Sonatype specific deployment configuration (from oss-parent POM) -->
		<profile>
			<id>jb-sonatypeReleaseConfiguration</id>
			<activation>
				<property>
					<name>jbPerformRelease</name>
				</property>
			</activation>
			
			<build>
				<plugins>
					<plugin>
						<groupId>org.apache.maven.plugins</groupId>
						<artifactId>maven-enforcer-plugin</artifactId>
						<executions>
							<execution>
								<id>enforce-maven</id>
								<goals>
									<goal>enforce</goal>
								</goals>
								<configuration>
									<rules>
										<requireMavenVersion>
											<version>(,2.1.0),(2.1.0,2.2.0),(2.2.0,)</version>
											<message>Maven 2.1.0 and 2.2.0 produce incorrect GPG signatures and checksums respectively.</message>
										</requireMavenVersion>
									</rules>
								</configuration>
							</execution>
						</executions>
					</plugin>
				</plugins>
			</build>
		</profile>
		
		<!-- Automatically close, release and drop the release artifacts on Sonartype -->
		<profile>
			<id>jb-sonatypeStageAutomatically</id>
			<activation>
				<property>
					<name>jbStageAutomatically</name>
					<value>true</value>
				</property>
			</activation>
			<build>
				<plugins>
					<plugin>
						<groupId>org.sonatype.plugins</groupId>
   		 				<artifactId>nexus-staging-maven-plugin</artifactId>
						<version>${version.nexus-staging-maven-plugin}</version>
						<extensions>true</extensions>
						<configuration>
							<serverId>sonatype-nexus-staging</serverId>
							<nexusUrl>https://oss.sonatype.org</nexusUrl>
							<!-- Automatically release and drop after close -->
							<autoReleaseAfterClose>true</autoReleaseAfterClose>
							<autoDropAfterRelease>true</autoDropAfterRelease>
						</configuration>
					</plugin>
				</plugins>
			</build>
		</profile>

		<!-- make sure the pom with the overridden version is used for signing and deploying -->
		<profile>
			<id>version.override</id>
			<activation>
				<property>
					<name>version.override</name>
				</property>
			</activation>
			<build>
				<plugins>
					<!-- configure the custom install plugin for lightweight release builds -->
					<!-- TODO: this plugin should be moved to ju, too -->
					<plugin>
						<groupId>ch.rotscher.maven.plugins</groupId>
						<artifactId>install-custom-version-plugin</artifactId>
						<version>0.3.4-pre-3</version>
						<executions>
							<execution>
								<id>version-override</id>
								<phase>package</phase>
								<goals>
									<goal>version-override</goal>
								</goals>
							</execution>
						</executions>
					</plugin>
				</plugins>
			</build>
		</profile>
		<!-- Set the version of the build to a the value of the buildId property -->
		<profile>
			<id>jb-setBuildVersion</id>
    	
    		<activation>
    			<property>
    				<name>jbBuildId</name>
    			</property>
    		</activation>
    	
    		<build>
    			<plugins>
    				<plugin>
    					<groupId>org.codehaus.mojo</groupId>
						<artifactId>versions-maven-plugin</artifactId>
    					<configuration>
    						<newVersion>${project.version}-${jbBuildId}</newVersion>
    					</configuration>
    				</plugin>
    			</plugins>
    		</build>
    	</profile>
	</profiles>
</project>