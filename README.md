Overview
========

JB stands for *Java Base* and contains basic artifacts that can be used for all InfTec Java projects. It includes funcionality to ready projects for deployment to the Central repository.

Build
=====

Profiles
--------

JB contains the following profiles:

* Activated by property _jbPerformRelease=sonatype_ (requirements to deploy to Maven Central):
	* *jb-sonatypeDistribution*: Define sonatype Snapshot and Release repository
	* *jb-attachSourceArtifacts*: Include sources (JAR as well as test JAR)
	* *jb-signReleaseArtifacts*: Signs artifacts using GnuPG. Default encrypted passphrase can be overridden with property *jbGpgPassphrase*
	* *jb-attachJavadocArtifacts*: Creates Javadoc artifacts
	* *jb-sonatypeReleaseConfiguration*: Include sonatype specific release configuration (like Maven version enforcement)
* Activated by property _jbPerformRelease=inftec_ (requirements to deploy to Inftec Maven Repository):
	* *jb-inftecDistribution*: Define inftec Snapshot and Release repository
	* *jb-attachSourceArtifacts*: Include sources (JAR as well as test JAR)
	* *jb-signReleaseArtifacts*: Signs artifacts using GnuPG. Default encrypted passphrase can be overridden with property *jbGpgPassphrase*
	* *jb-attachJavadocArtifacts*: Creates Javadoc artifacts
	* *jb-sonatypeReleaseConfiguration*: Include sonatype specific release configuration (like Maven version enforcement)
* Activated by property _jbStageAutomatically=true_
	* *jb-sonatypeStageAutomatically*: Automatically closes, releases and drops the release artifacts on Sonartype
* Activated by property _jbBuildId_
	* *jb-setBuildVersion*: Sets the build version to version-buildId when using versions:set

Deployment
==========

JU is set up for deployment to Sonatype (https://oss.sonatype.org). From there, it can be synched to the maven central repository
(http://search.maven.org).

Before a release is published to Sonatype and maven central, a pre-release to the Inftec Maven Repository (https://mvn.inftec.ch) can be done!

* Deploy Snapshot:
	* Make sure the version is a snapshot version, e.g. 1.0-SNAPSHOT
	* run _mvn clean deploy_
* Deploy Release:
	* Make sure the version is a release version, e.g. 1.0-Final (only when pushing to master branch), otherwise a SNAPSHOT-version is ok
	* Releases to Sonatype: run _mvn clean deploy -DjbPerformRelease=sonatype -DjbStageAutomatically=true -Dversion.override -Dversion.override.strict=true -Dclassifier.type=RELEASE_
	* Releases to Inftec:   run _clean deploy -DjbPerformRelease=inftec -Dversion.override -Dversion.override.strict -Dclassifier.type=DEVELOP_
	* Parameters that may be passed using maven -D flags, e.g. -Dgpg.executable=/gpg
		* *version.override*: When the release should be created with a generated version, based on the version within the pom. *version.override.strict* and *classifier.type* are optional in case *version.override* is not used
		* *gpg.executable*: Path to GPG executable, e.g. _C:\Program Files (x86)\gnu\GnuPG\gpg.exe_
		* *gpg.keyname*: Key to sign with, e.g. A4A7141C for info@inftec.ch key
		* *gpg.passphrase*: Passphrase of the private key
		* *gpg.publicKeyring*: Path to public keyring, defaults to pubring.gpg from home directory
		* *gpg.secretKeyring*: Path to secret keyring, defaults to secring.gpg from home directory
	* Enter GPG private key password
	* Wait for build to succeed
	* When _jbStageAutomatically=true_ was used, the artifacts should be provisioned automatically. Otherwise, provision them manually
   by going to https://oss.sonatype.org:
		* Select _Staging Repositories_ and look for a repository with profile _ch.inftec_
		* Select it, verify it and click _Close_
		* When tests are all successful, refresh and click _Release_ to release the artifacts to Maven Central repository

*Note*: In order to perform deployments, the Sonartype servers have to be configured in the settings.xml. Release artifacts
need to be signed in addition. For more info, see: https://inftec.atlassian.net/wiki/display/TEC/Maven#Maven-DeploytoCentral

### Example Deployment commands

* Deploy release locally: mvn verify -DjbPerformRelease=sonatype -Dgpg.keyname=A4A7141C "-Dgpg.passphrase=xxx"
* Deploy release locally, automatically staging: mvn verify -DjbPerformRelease=sonatype -DjbStageAutomatically=true -Dgpg.keyname=A4A7141C "-Dgpg.passphrase=xxx"

GPG Key
-------

InfTec GmbH has a generic GPG key to sign releases:

* Expires: 2014-12-14 (after 1 year, created 2013-12-14)
* User ID: InfTec (InfTec GmbH) <info@inftec.ch>
	* Real name: InfTec
	* Email address: info@inftec.ch
	* Comment: InfTec GmbH
	* Passphrase: ...
